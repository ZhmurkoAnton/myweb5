function ClickButton() {
    let r = document.getElementById("result");
    let price = document.getElementById("price").value;
    if (!(/^0$|^[1-9][0-9]*$/).test(price)) {
        r.innerHTML = "Неверная цена!";
        return;
    }
    price = parseInt(price);
    let quantity = document.getElementById("quantity").value;
    if (!(/^0$|^[1-9][0-9]*$/).test(quantity)) {
        r.innerHTML = "Неверное количество!";
        return;
    }
    quantity = parseInt(quantity);
    r.innerHTML = price * quantity;
}

window.addEventListener("DOMContentLoaded", function () {
    let b = document.getElementById("clickme");
    b.addEventListener("click", ClickButton);
});